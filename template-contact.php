<?php
/* Template Name: Contact */
get_header();
?>
<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="content" class="site-content">
		<?php while (have_posts()) : the_post(); ?>
		<h1 class="title"><?php the_title() ?></h1>
		<?php the_content(); ?>
		<?php endwhile; ?>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3279.1978608582262!2d-86.69047590000001!3d34.72540669999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88626ecd97331c8f%3A0x7cf9edb68b791e38!2s601+Genome+Way+NW%2C+Huntsville%2C+AL+35806!5e0!3m2!1sen!2sus!4v1408941821554" width="100%" height="250" frameborder="0" style="border:0"></iframe>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>