<?php
/* Template Name: About */
get_header();
?>
<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="content" class="site-content">
		<?php while (have_posts()) : the_post(); ?>
		<h1 class="title"><?php the_title(); ?> <?php echo get_theme_mod( 'custom_option_three', '' ); ?></h1>
		<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>