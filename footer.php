<?php
/**
* The template for displaying the footer.
*/
?>
</div><!-- close .row -->
</div><!-- close .container -->
</div><!-- close .main-content -->
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="site-footer-inner col-sm-12">
				<div class="footer-site-info">
					HudsonAlpha Institute for Biotechnology. | 601 Genome Way | Huntsville AL 35806
				</div><!-- close .site-info -->
				<ul class="inline-list">
					<li class="item">
						<a href="https://twitter.com/hudsonalpha" class="link twitter"></a>
					</li>
					<li class="item">
						<a href="https://www.facebook.com/HudsonAlpha" class="link facebook"></a>
					</li>
					<!-- <li class="item">
						<a href="#" class="link linkedin"></a>
					</li> -->
				</ul>
			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->
<?php wp_footer(); ?>
</body>
</html>