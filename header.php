<?php
/* The Header for our theme. */
?><!DOCTYPE html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>
	<nav class="site-upper-header">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div id="upper-header-address"><a href="">&#60; Go Back to Main Site.</a>   601 Genome Way | Huntsville, Alabama 35806</div>
				</div>
				<div class="pull-right">
					<div id="upper-header-phone-email"><?php echo get_theme_mod( 'custom_option_two', 'Default Value Two' ); ?> | <a href="mailto:<?php echo get_theme_mod( 'custom_option_one', 'Default Value' ); ?>" class="link"><?php echo get_theme_mod( 'custom_option_one', 'Default Value' ); ?></a></div>
				</div>
			</div>
		</div>
	</nav>
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"
						title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>"
						height="<?php echo get_custom_header()->height; ?>" alt="">
					</a>
					<div class="site-branding">
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"
							title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
						rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>
					</div>
				</div>
				<div class="logo col-xs-12 col-sm-2 col-md-6 col-lg-2 pull-right"></div>
			</div>
		</div>
		<!-- .container -->
	</header>
	<!-- #masthead -->
	<nav class="site-navigation">
		<div class="container">
			<div class="row">
				<div class="site-navigation-inner col-sm-12">
					<div class="navbar navbar-default">
						<div class="navbar-header">
							<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
							<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-responsive-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
							<!-- The WordPress Menu goes here -->
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'primary',
									// 'theme_location' => 'primary-menu',
									// 'theme_location' => 'main-menu',
									'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
									'menu_class'      => 'nav navbar-nav',
									'fallback_cb'     => '',
									'menu_id'         => 'main-menu',
									'walker'          => new wp_bootstrap_navwalker()
								)
							); ?>
						</div>
						<!-- .navbar -->
					</div>
				</div>
			</div>
			<!-- .container -->
		</nav>
		<!-- .site-navigation -->
		<div class="main-content">
			<div class="container">
				<div class="row">