// Handle switch in administrator panel

(function ( $ ) {
	"use strict";

	$(function () {

		// consider adding an id to your table,
		// just incase a second table ever enters the picture..?

		var publications = $(".publications .publication");
		var news = $(".news .item");

		if (publications.length > 0) {

			var items = publications;
			var perPage = 10;

		} else if (news.length > 0) {

			var items = news;
			var perPage = 5;

		}

		var numItems = items.length;

		// only show the first 2 (or "first per_page") items initially
		items.slice(perPage).hide();

		// now setup your pagination
		// you need that .pagination-page div before/after your table
		$(".paginator").pagination({
			items: numItems,
			itemsOnPage: perPage,
			prevText : 'previous page',
			nextText : 'next page',
			onPageClick: function(pageNumber) { // this is where the magic happens
				// someone changed page, lets hide/show trs appropriately
				var showFrom = perPage * (pageNumber - 1);
				var showTo = showFrom + perPage;

				items.hide() // first hide everything, then show for the new page
					.slice(showFrom, showTo).show();
					$("html, body").animate({ scrollTop: 0 }, "slow");
					// window.scrollTo(x-coord, y-coord);

        		// $('html,body').animate({scrollTop: $(this).offset().top}, 0);
			}
		});

	});

}(jQuery));