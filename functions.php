<?php

add_theme_support( 'post-thumbnails' );

function _tk_scripts() {



// if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
// function my_jquery_enqueue() {
//    wp_deregister_script('jquery');
//    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false, null);
//    wp_enqueue_script('jquery');
// }

  // wp_deregister_script('jquery');
  // wp_register_script('jquery', ("http://code.jquery.com/jquery-latest.min.js"), false, '');
  wp_enqueue_script('jquery');



	// load bootstrap css
	wp_enqueue_style( '_tk-bootstrap', get_template_directory_uri() . '/includes/resources/bootstrap/css/bootstrap.css' );
	// load _tk styles
	wp_enqueue_style( '_tk-style', get_stylesheet_uri() );
	// load bootstrap js
	wp_enqueue_script( '_tk-bootstrapjs', get_template_directory_uri() . '/includes/resources/bootstrap/js/bootstrap.js', array( 'jquery' ) );

	// load bootstrap wp js
	wp_enqueue_script( '_tk-bootstrapwp', get_template_directory_uri() . '/includes/js/bootstrap-wp.js', array( 'jquery' ) );

	// load boot.js basic js init script - this controls pagination
	wp_enqueue_script( 'boot', get_template_directory_uri() . '/includes/js/boot.js', array( 'jquery' ) );

	wp_enqueue_script( 'simplepagination', get_template_directory_uri() . '/includes/js/simplePagination.js', array( 'jquery' ) );

	wp_enqueue_script( '_tk-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );
}
add_action( 'wp_enqueue_scripts', '_tk_scripts' );

/* Load custom WordPress nav walker. */
require get_template_directory() . '/includes/bootstrap-wp-navwalker.php';
add_action( 'after_setup_theme', 'remove_admin_bar' );
function remove_admin_bar() {
	show_admin_bar( false );
	if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
		show_admin_bar( false );
	}
}

/*  Register Custom Post Types */

add_action( 'init', 'cpt_register' );
function cpt_register() {

	/*  Register the News Custom Post Type */
	register_post_type( 'news', 
		array(
			'labels'       => array(
				'name'               => __( 'News', 'html5blank' ), // Rename these to suit
				'singular_name'      => __( 'News', 'html5blank' ),
				'add_new'            => __( 'Add News', 'html5blank' ),
				'add_new_item'       => __( 'Add News', 'html5blank' ),
				'edit'               => __( 'Edit', 'html5blank' ),
				'edit_item'          => __( 'Edit News', 'html5blank' ),
				'new_item'           => __( 'New', 'html5blank' ),
				'view'               => __( 'View', 'html5blank' ),
				'view_item'          => __( 'View', 'html5blank' ),
				'search_items'       => __( 'Search', 'html5blank' ),
				'not_found'          => __( 'No News found', 'html5blank' ),
				'not_found_in_trash' => __( 'No News found in Trash', 'html5blank' )
			),
			'public'       => true,
			'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
			'has_archive'  => false,
			'supports'     => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'
			),
		)
	);
	register_taxonomy(
		'ntags',
		'news',
		array(
			'label'        => __( 'Tags' ),
			// 'rewrite'      => array( 'slug' => 'news/ntags' ),
			'hierarchical' => false
		)
	);
	register_taxonomy(
		'ncategory',
		'news',
		array(
			'label'        => __( 'Category' ),
			'rewrite'      => array( 'slug' => 'news/ncategory' ),
			'hierarchical' => true
		)
	);

	/*  Register the Lab Members Custom Post Type */
	register_post_type( 'lab_members', // Register Custom Post Type
		array(
			'labels'       => array(
				'name'               => __( 'Lab Members', 'html5blank' ), // Rename these to suit
				'singular_name'      => __( 'Lab Member', 'html5blank' ),
				'add_new'            => __( 'Add Lab Member', 'html5blank' ),
				'add_new_item'       => __( 'Add Lab Member', 'html5blank' ),
				'edit'               => __( 'Edit', 'html5blank' ),
				'edit_item'          => __( 'Edit Lab Member', 'html5blank' ),
				'new_item'           => __( 'New', 'html5blank' ),
				'view'               => __( 'View', 'html5blank' ),
				'view_item'          => __( 'View', 'html5blank' ),
				'search_items'       => __( 'Search', 'html5blank' ),
				'not_found'          => __( 'No members found', 'html5blank' ),
				'not_found_in_trash' => __( 'No members found in Trash', 'html5blank' )
			),
			'public'       => true,
			'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
			'has_archive'  => true,
			'supports'     => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'
			),
		)
	);

	register_post_type( 'researchprojects', // Register Custom Post Type
		array(
			'labels'       => array(
				'name'               => __( 'Research projects', 'html5blank' ), // Rename these to suit
				'singular_name'      => __( 'Research project', 'html5blank' ),
				'add_new'            => __( 'Add Research project', 'html5blank' ),
				'add_new_item'       => __( 'Add Research project', 'html5blank' ),
				'edit'               => __( 'Edit', 'html5blank' ),
				'edit_item'          => __( 'Edit research project', 'html5blank' ),
				'new_item'           => __( 'New', 'html5blank' ),
				'view'               => __( 'View', 'html5blank' ),
				'view_item'          => __( 'View', 'html5blank' ),
				'search_items'       => __( 'Search', 'html5blank' ),
				'not_found'          => __( 'No research projects found', 'html5blank' ),
				'not_found_in_trash' => __( 'No research projects found in Trash', 'html5blank' )
			),
			'public'       => true,
			'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
			'has_archive'  => true,
			'supports'     => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'
			),
		) 
	);
}
/** For Editing of Lab members - Adds a box to the main column on the Post and Page edit screens:

* Add a "deck" (aka subhead) meta box to post page(s) and position it
* under the title.
*
* @see http://codex.wordpress.org/Function_Reference/add_meta_box
* @see http://wordpress.org/extend/ideas/topic/add-meta-box-to-multiple-post-types
* @see https://github.com/Horttcore/WordPress-Subtitle
* @see http://codex.wordpress.org/Function_Reference/wp_nonce_field
*/
# 
function lab_members_cv_desk( $post_type ) {
	# Allowed post types to show meta box:
	$post_types = array( 'lab_members' );
	if ( in_array( $post_type, $post_types ) ) {
		# Add a meta box to the administrative interface:
		add_meta_box(
			'cv-meta-box', // HTML 'id' attribute of the edit screen section.
			'CV details',              // Title of the edit screen section, visible to user.
			'cv_meta_box_func', // Function that prints out the HTML for the edit screen section.
			$post_type,          // The type of Write screen on which to show the edit screen section.
			'advanced',          // The part of the page where the edit screen section should be shown.
			'high'               // The priority within the context where the boxes should show.
		);
	}
}
# Callback that prints the box content:
function cv_meta_box_func( $post ) {
	// Get the location data if its already been entered
	$position  = get_post_meta( $post->ID, 'cv_position', true );
	$email     = get_post_meta( $post->ID, 'cv_email', true );
	$telephone = get_post_meta( $post->ID, 'cv_telephone', true );
	$past_member = get_post_meta( $post->ID, 'cv_past_member', true );
	# Form field to display:
	?>
	<p>Job title:</p>
	<input type="text" placeholder="Job title" name="cv_position" value="<?php print esc_attr( $position ); ?>"
	class="widefat"/>
	<p>Email:</p>
	<input type="text" placeholder="Email" name="cv_email" value="<?php print esc_attr( $email ); ?>" class="widefat"/>
	<p>Telephone:</p>
	<input type="text" placeholder="Telephone" name="cv_telephone" value="<?php print esc_attr( $telephone ); ?>"
	class="widefat"/>

	<!-- implement a check box to indicate past member in the future -->
	<!-- <p>Past Member?:</p>
	<input type="checkbox" placeholder="Present" name="cv_past_member" value="<?php print esc_attr( $past_member ); ?>"
	class="widefat"/> -->


	<?php

	# Display the nonce hidden form field:
	wp_nonce_field(
		plugin_basename( __FILE__ ), // Action name.
		'cv-meta-box'        // Nonce name.
	);
}

/**
* @see http://wordpress.stackexchange.com/a/16267/32387
*/
# Save our custom data when the post is saved:
function lab_members_cv_desk_save_postdata( $post_id ) {
	# Is the current user is authorised to do this action?
	if ( ( ( $_POST['post_type'] === 'lab_members' ) && current_user_can( 'edit_page', $post_id ) || current_user_can( 'edit_post', $post_id ) ) ) { // If it's a page, OR, if it's a post, can the user edit it?
		# Stop WP from clearing custom fields on autosave:
		if ( ( ( ! defined( 'DOING_AUTOSAVE' ) ) || ( ! DOING_AUTOSAVE ) ) && ( ( ! defined( 'DOING_AJAX' ) ) || ( ! DOING_AJAX ) ) ) {
			# Nonce verification:
			if ( wp_verify_nonce( $_POST['cv-meta-box'], plugin_basename( __FILE__ ) ) ) {
				# Get the posted deck:
				$values = array();
				$names  = array( 'cv_position', 'cv_email', 'cv_telephone', 'cv_past_member' );
				// Get the location data if its already been entered
				$position  = sanitize_text_field( $_POST['cv_position'] );
				$email     = sanitize_text_field( $_POST['cv_email'] );
				$telephone = sanitize_text_field( $_POST['cv_telephone'] );
				$past_member = sanitize_text_field( $_POST['cv_past_member'] );
				array_push( $values, $position, $email, $telephone, $past_member );
				foreach ( $values as $key => $value ) {
					if ( $value !== '' ) {
						# Deck exists, so add OR update it:
						update_post_meta( $post_id, $names[ $key ], $value );
					} else {
						# Deck empty or removed:
						delete_post_meta( $post_id, $names[ $key ] );
					}
				}
			}
		}
	}
}

# Define the custom box:
add_action( 'add_meta_boxes', 'lab_members_cv_desk' );
# Do something with the data entered:
add_action( 'save_post', 'lab_members_cv_desk_save_postdata' );

/**
* @see http://wordpress.stackexchange.com/questions/36600
* @see http://wordpress.stackexchange.com/questions/94530/
*/

# Now move advanced meta boxes after the title:
function lab_members_cv_move_deck() {
	# Get the globals:
	global $post, $wp_meta_boxes;
	# Output the "advanced" meta boxes:
	do_meta_boxes( get_current_screen(), 'advanced', $post );
	# Remove the initial "advanced" meta boxes:
	unset( $wp_meta_boxes[ get_post_type( $post ) ]['advanced'] );
}
add_action( 'edit_form_after_title', 'lab_members_cv_move_deck' );

/**
* Add a "deck" (aka subhead) meta box to post page(s) and position it
* under the title.
*
* @see http://codex.wordpress.org/Function_Reference/add_meta_box
* @see http://wordpress.org/extend/ideas/topic/add-meta-box-to-multiple-post-types
* @see https://github.com/Horttcore/WordPress-Subtitle
* @see http://codex.wordpress.org/Function_Reference/wp_nonce_field
*/

/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'bt_flush_rewrite_rules' );
/* Flush your rewrite rules */
function bt_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// add a custom header - needed for custom header options
function _tk_custom_header_setup() {
	if ( function_exists( 'add_theme_support' ) ) {
		add_theme_support( 'custom-header', apply_filters( '_tk_custom_header_args', array(
			// 'default-image'          => '',
			// 'default-text-color'     => '000',
			// 'width'                  => 1170,
			// 'height'                 => 250,
			// 'flex-height'            => true,
			// 'wp-head-callback'       => '_tk_header_style',
			// 'admin-head-callback'    => '_tk_admin_header_style',
			// 'admin-preview-callback' => '_tk_admin_header_image',
		) ) );
	}
}
add_action( 'after_setup_theme', '_tk_custom_header_setup' );

// set custom options in the header file - for phone number
add_action('admin_head', 'save_my_custom_options');

function save_my_custom_options() {
	if ( isset( $_POST['custom_option_one'] ) && isset( $_POST['custom_option_two'] ) && isset( $_POST['custom_option_three'] ) ) {
		// validate the request itself by verifying the _wpnonce-custom-header-options nonce
		// (note: this nonce was present in the normal Custom Header form already, so we didn't have to add our own)
		check_admin_referer( 'custom-header-options', '_wpnonce-custom-header-options' );

		// be sure the user has permission to save theme options (i.e., is an administrator)
		if ( current_user_can('manage_options') ) {

			// NOTE: Add your own validation methods here
			set_theme_mod( 'custom_option_one', $_POST['custom_option_one'] );
			set_theme_mod( 'custom_option_two', $_POST['custom_option_two'] );
			set_theme_mod( 'custom_option_three', $_POST['custom_option_three'] );
		}
	}
	return;
}

// add our custom header options hook
add_action('custom_header_options', 'ha_custom_header_options');
 
/* Adds two new text fields, custom_option_one and custom_option_two to the Custom Header options screen */
function ha_custom_header_options() {
?>
<table class="form-table">
	<tbody>
		<tr valign="top" class="hide-if-no-js">
			<th scope="row"><?php _e( 'Header Email Address:' ); ?></th>
			<td>
				<p>
					<input type="text" name="custom_option_one" id="custom_option_one" value="<?php echo esc_attr( get_theme_mod( 'custom_option_one', 'info@hudsonalpha.org' ) ); ?>" />
				</p>
			</td>
		</tr>
		<tr valign="top" class="hide-if-no-js">
			<th scope="row"><?php _e( 'Header Phone Number:' ); ?></th>
			<td>
				<p>
					<input type="text" name="custom_option_two" id="custom_option_two" value="<?php echo esc_attr( get_theme_mod( 'custom_option_two', '256.327.0400' ) ); ?>" />
				</p>
			</td>
		</tr>

		<tr valign="top" class="hide-if-no-js">
			<th scope="row"><?php _e( 'Researcher Name:' ); ?></th>
			<td>
				<p>
					<input type="text" name="custom_option_three" id="custom_option_three" value="<?php echo esc_attr( get_theme_mod( 'custom_option_three', '' ) ); ?>" />
				</p>
			</td>
		</tr>
	</tbody>
</table>
<?php
} // end my_custom_image_options

/* add styling to default wordpress pagination */
// add_filter('wp_link_pages', 'bootstrap_wp_link_pages');
// function bootstrap_wp_link_pages($wp_links){
// 	global $post;
 
// 	// Generate current page base url without pagination.
// 	$post_base = trailingslashit( get_site_url(null, $post->post_name) );
 
// 	$wp_links = trim(str_replace(array('<p>Pages: ', '</p>'), '', $wp_links));
 
// 	// Get out of here ASAP if there is no paging.
// 	if ( empty($wp_links) )
// 		return '';
 
// 	// Split on spaces
// 	$splits = explode(' ', $wp_links );
// 	$links = array();
// 	$current_page = 1;
 
// 	// Since links are now split up such that <a and href=".+" are seperate...
// 	// loop over split array and correct links.
// 	foreach( $splits as $key => $split ){
// 		if( is_numeric($split) ) {
// 			$links[] = $split;
// 			$current_page = $split;
// 		} else if ( strpos($split, 'href') === false ) {
// 			$links[] = $split . ' ' . $splits[$key + 1];
// 		}
// 	}
 
// 	$num_pages = count($links);
// 	// Output pagination
// 	$output = '';
// 	$output .= '<ul class="pagination">';
// 	$output .= "<li><a href=\"{$post_base}\">&lt;&lt;</a></li>";
 
// 	if ( $current_page == 1 )
// 		$output .= '<li class="disabled"><a>';
// 	else
// 		$output .= '<li><a href="' . $post_base . ($current_page - 1) . '">';
// 		$output .= '&lt;</a></li>';	// end the li. No reason to duplicated this in both conditionals.
 
// 	foreach( $links as $key => $link ) {
// 		if ( is_numeric($link) ) {
// 			$temp_key = $key + 1;
// 			$output .= "<li class=\"active\"><a href=\"{$post_base}{$temp_key}\">{$temp_key}</a></li>";
// 		}
// 		else {
// 			$output .= "<li>{$link}</li>";
// 		}
// 	}

// 	if ( $current_page == $num_pages )
// 		$output .= '<li class="disabled"><a>';
// 	else
// 		$output .= '<li><a href="' . $post_base . ($current_page + 1) . '">';
// 		$output .= '&gt;</a></li>';	// end the li. No reason to duplicated this in both conditionals.
// 		$output .= "<li><a href=\"{$post_base}{$num_pages}\">&gt;&gt;</a></li>";
// 		$output .= '</ul>';
// 		return $output;
// }

// Add Menu Locations
function register_my_menus() {
	register_nav_menus(
		array(  
			'primary' => ( 'main-menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );
