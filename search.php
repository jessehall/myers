<?php
/* The template for displaying Search Results pages */
get_header(); ?>

<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="content" class="site-content">
		<?php if ( have_posts() ) : ?>
			<div id="search-results-header">
				<h3>Search results for "<?php echo get_search_query(); ?>":</h3>
			</div>
			<br />
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
						<div class="search-results">
							<h4>
								<a href="<?php the_permalink(); ?>">
									<?php the_title();  ?>
								</a>
							</h4>
							<p><?php the_excerpt(); ?></p>
						</div>
				<?php endwhile; ?>
				<?php get_search_form(); ?>
		<?php else : ?>
			<p>
				Sorry, but nothing matched your search criteria. Please try again with some different keywords.
			</p>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
