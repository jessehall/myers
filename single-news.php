<?php get_header(); ?>
<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="content" class="site-content">
		<?php while ( have_posts() ) : the_post(); ?>
		<h1 class="title"><?php the_title() ?></h1>
		<div><?php the_content(); ?></div>
		<?php endwhile; ?>
	</div>
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>/news">Back to News</a>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
